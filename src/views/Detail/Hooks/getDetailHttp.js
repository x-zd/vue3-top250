import axios from "axios"
import { onActivated, ref } from 'vue'
import { useRoute } from 'vue-router'
export const getDetailHttp = () => {
    const result = ref({});
    const $route = useRoute();
    onActivated(() => {
        var { id } = $route.query;
        var url = import.meta.env.VITE_BASE_URL;
        axios.get(`${url}/films/detail?id=${id}`).then(res => {
            result.value = res.data.result;
        })
    })
    return { result }
}
import { ref, onActivated,onDeactivated } from 'vue'
export const useNav = () => {
    let opacityValue = ref(0);
    const handleScroll = () => {
        let scrollTop = window.scrollY;
        /* 达到300px完全显示 */
        let opacity = scrollTop / 300;
        if (opacity > 1) {
            opacity = 1;
        }
        console.log(opacity)
        opacityValue.value = opacity;
    }
    onActivated(() => {
        window.addEventListener("scroll",
            handleScroll)
    })
    onDeactivated(()=>{
        /* 在页面被缓存的时候,执行window事件的解绑 */
        window.removeEventListener("scroll",handleScroll)
    })
    return { opacityValue };
}
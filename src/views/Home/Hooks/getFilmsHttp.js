import { onMounted, ref } from 'vue'
import axios from 'axios'
export const getFilmsHttp = () => {
    const movies = ref([])
    onMounted(() => {
        var url = import.meta.env.VITE_BASE_URL;
        axios.get(`${url}/films/top250`).then(res => {
            movies.value = res.data.result
        })
    })
    return { movies }
}